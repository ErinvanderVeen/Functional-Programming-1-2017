> {-# LANGUAGE UnicodeSyntax, DeriveGeneric #-}
> module Database
> where
> import GHC.Generics
> import Unicode

> data Person = Person { name ∷ String
>                      , age ∷ Integer
>                      , favouriteCourse ∷ String
>                      } deriving Show

> frits, peter, ralf, erin ∷ Person
> frits  = Person { name = "Frits", age = 33, favouriteCourse = "Algorithms and Data Structures" }
> peter  = Person { name = "Peter", age = 57, favouriteCourse = "Imperative Programming" }
> ralf   = Person { name = "Ralf",  age = 33, favouriteCourse = "Functional Programming" }
> erin   = Person { name = "Erin",  age = 22, favouriteCourse = "Functional Programming in Clean" }

> students   ∷  [Person]
> students   =  [frits, peter, ralf, erin]

> showPerson ∷ Person → String
> showPerson = show

> twins ∷ Person → Person → Bool
> twins p1 p2 = age p1 == (age p2)

> increaseAge ∷ Person → Person
> increaseAge p = p { age = succ (age p) }

> promote :: Person -> Person
> promote p = p { name = "dr " ++ (name p) }

map (increaseAge . increaseAge) students
map promote students
filter ((==) "Frits" . name) students
filter ((==) "Functional Programming" . favouriteCourse) students
filter (\p -> age p < 30 && age p >= 20) students
filter (\p -> "Functional Programming" == (favouriteCourse p) && age p < 30 && age p >= 20) students
filter (\p -> "Imperative Programming" == (favouriteCourse p) || age p < 30 && age p >= 20) students
