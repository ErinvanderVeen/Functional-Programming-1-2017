> {-# LANGUAGE UnicodeSyntax #-}
> module Database
> where
> import GHC.Generics
> import Data.Tuple.Extra
> import Data.Tuple.Select
> import Unicode

> type Person  =  (Name, Age, FavouriteCourse)
>
> type Name             =  String
> type Age              =  Integer
> type FavouriteCourse  =  String

> frits, peter, ralf ∷ Person
> frits  =  ("Frits",  33,  "Algorithms and Data Structures")
> peter  =  ("Peter",  57,  "Imperative Programming")
> ralf   =  ("Ralf",   33,  "Functional Programming")
> erin   =  ("Erin",   22,  "Functional Programming in Clean")

> students   ∷  [Person]
> students   =  [frits, peter, ralf, erin]

> age ∷ Person → Age
> age (_n, a, _c)  =  a

> name ∷ Person → Name
> name = sel1

> favouriteCourse ∷ Person → FavouriteCourse
> favouriteCourse = sel3

> showPerson ∷ Person → String
> showPerson p = "Name: " ++ (show . name) p ++ "\nAge: " ++ (show . age) p ++ "\nFavouriteCourse: " ++ favouriteCourse p

> twins ∷ Person → Person → Bool
> twins p1 p2 = age p1 == (age p2)

> increaseAge ∷ Person → Person
> increaseAge (n, a, c) = (n, succ a, c)

